/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'i.1337co.de',
      },
    ],
  },
};

export default nextConfig;
