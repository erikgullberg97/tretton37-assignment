# Tretton37 - Employee Assignment 📝

A employee registry of people at Tretton37.
The interface displays a list of employees with their name, office and links to their socials.

The project is built using NextJs. It uses typescript for type safety and jest for testing.

This was made as a coding assignment for Tretton37.

A live deployment of this app lives at https://employee-assignment.vercel.app/

## User stories selected

**Responsive design**
- Responsive design is important for a good user experience and essential for webapps. Because I already usually take this into account it felt easy to implement it here as well.

**Modern CSS**
- While CSS as a whole is difficult to master. I'm in love with using flexbox for layouts and css variables for reusable styles. While not important to the user it makes the developer experience lots more fun

**Switch between grid and different view**
- This felt like an given feature to implement as i used flexbox for my layout which easily supports grid and list views.

**Available on a public url**
- Because I used NextJS I could easily deploy to a free vercel url making this story a great pick.

**Use Typescript or similar**
- This felt like an easy pick as I almost always use Typescript these days.

**Unit tests for existing functionality**
- I feel tests are essential for knowing that your code actually works as it should. I implemented tests as soon as I implemented components to ensure that they worked and that their behaviour didn't change when more features are added.

## Code design
The project is built using a simple src folder structure.
Inside the src folders there's
- app - Contains the routing and layout, using NextJs' app router.
- components / api / types / tests - Contains their respective domains

Styles are written in pure CSS and are mostly scoped to the components they are used in for clarity.

Each component is built with a single purpose and with minimal dependencies on other components to make them easy to test, change and replace.

## Package Decisions
**NextJS**
- I decided to use NextJs for this project as it is a fully featured and well supported framework for modern react applications. It takes care of a lot of modern web features out of the box.
- It also integrates well with Typescript, Jest & Vercel which I wanted to use for this project.
- Next does however come with some overhead and is a bit overkill for an app of this size.

**Typescript**
- Typescript is great for ensuring code quality and prevent errors.
- For me, its a standard to include it in new projects.

**Jest**
- Jest is a great and easy testing framework which works nicely with typescript and Next.
- It's easy to read, write and understand.

**Prettier**
- Prettier is just so good for ensuring standard code formatting. Can't go without it!

## Getting Started

### Prerequisites
- Node v20.13.1 or later
- API key for the Tretton37 API

### 1. Clone the repository

### 2. Install dependencies
`npm install`

### 3. Create env file
Create a `.env` file in the root of the project and add the following line:
```API_KEY=YOUR_API_KEY```

Replace `YOUR_API_KEY` with your working API key:

### 4. Now you're ready to go!
Run the project with `npm run dev`

Run tests with `npm run test`

## Notes and Improvements
- Due to a [bug](https://github.com/vercel/next.js/issues/65161) with Next and Jest at the time of writing. Jest needs to be run with the --silent flag to run the tests.
- Error handling is missing entirely. This is a big oversight and would be the first thing I would improve. Next somewhat prevents the app from crashing but no user friendly error handling is implmented.