import Employee from "@/types/employee";

const apiKey = process.env.API_KEY as string;

export default async function getEmployees(): Promise<Employee[]> {
  const res = await fetch("https://api.1337co.de/v3/employees", {
    headers: {
      Authorization: apiKey,
    },
  });

  return await res.json();
}
