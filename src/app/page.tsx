import styles from "./page.module.css";
import EmployeeList from "@/components/EmployeeList/EmployeeList";
import getEmployees from "@/api/eployees";

async function fetchEmployees() {
  return await getEmployees();
}

export default async function Home() {
  const employees = await fetchEmployees();
  return (
    <main className={styles.main}>
      <div className={styles.header}>
        <h2>The fellowship of the tretton37</h2>
      </div>
      <EmployeeList employees={employees} />
    </main>
  );
}
