import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import EmployeeList from "@/components/EmployeeList/EmployeeList";
import Employee from "@/types/employee";

function generateRandomEmployees(): Employee[] {
  const numEmployees = Math.floor(Math.random() * 25) + 1;

  return Array.from({ length: numEmployees }, (_, i) => ({
    name: `Employee ${i + 1}`,
    email: `employee${i + 1}@example.com`,
    phoneNumber: `+1333337${i + 1}`,
    office: "Lund",
    manager: null,
    orgUnit: "/Employees",
    mainText: `<p>Employee ${i + 1} was here</p>`,
    gitHub: `https://github.com/employee${i + 1}`,
    twitter: `https://x.com/employee${i + 1}`,
    stackOverflow: null,
    linkedIn: `/in/employee-${i + 1}`,
    imagePortraitUrl: null,
    imageWallOfLeetUrl: null,
    highlighted: false,
    published: false,
    primaryRole: "Fullstack Engineer",
    secondaryRole: null,
    area: "Engineering",
  }));
}

describe("EmployeeList", () => {
  const employees = generateRandomEmployees();

  it("renders component", () => {
    render(<EmployeeList employees={employees} />);
  });

  it("renders correct number of EmployeeCard components", () => {
    const { getAllByTestId } = render(<EmployeeList employees={employees} />);
    const cards = getAllByTestId("employee-card");
    expect(cards.length).toBe(employees.length);
  });

  it("changes display mode when radio buttons are clicked", () => {
    render(<EmployeeList employees={employees} />);
    const gridRadioButton = screen.getByLabelText("Grid");
    const spotlightRadioButton = screen.getByLabelText("Spotlight");

    expect(gridRadioButton).toBeChecked();
    expect(spotlightRadioButton).not.toBeChecked();

    fireEvent.click(spotlightRadioButton);

    expect(spotlightRadioButton).toBeChecked();
    expect(gridRadioButton).not.toBeChecked();

    const spotlightList = document.querySelector(".spotlightEmployeeContainer");
    expect(spotlightList).toBeInTheDocument();
  });
});
