import getEmployees from "../api/eployees";
import type Employee from "@/types/employee";

const mockEmployeeData: Employee[] = [
  {
    name: "Erik Gullber",
    email: "me@erikgullberg.se",
    phoneNumber: "+46734108585",
    office: "Lund",
    manager: null,
    orgUnit: "/Employees",
    mainText: "<p>Erik was here</p>",
    gitHub: "https://github.com/Erma32",
    twitter: null,
    stackOverflow: null,
    linkedIn: "/in/erik-gullberg-a57b70167/",
    imagePortraitUrl: null,
    imageWallOfLeetUrl: null,
    highlighted: false,
    published: false,
    primaryRole: "Fullstack Engineer",
    secondaryRole: null,
    area: "Engineering",
  },
];

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(mockEmployeeData),
  }),
) as jest.Mock;

describe("getEmployees", () => {
  it("should get and return employee data", async () => {
    const employees = await getEmployees();

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith("https://api.1337co.de/v3/employees", {
      headers: {
        Authorization: process.env.API_KEY as string,
      },
    });
    expect(employees).toEqual(mockEmployeeData);
  });

  it("should not crash when encountering error", async () => {
    (fetch as jest.Mock).mockImplementationOnce(() => {
      throw new Error("Error while fetching data");
    });

    await expect(getEmployees()).rejects.toThrow("Error while fetching data");
  });
});
