import React from "react";
import { render, screen } from "@testing-library/react";
import EmployeeCard from "@/components/EmployeeCard/EmployeeCard";
import Employee from "@/types/employee";
import "@testing-library/jest-dom";

describe("EmployeeCard", () => {
  const testEmployee: Employee = {
    name: "Erik Gullberg",
    email: "me@erikgullberg.se",
    phoneNumber: "+46734108585",
    office: "Lund",
    manager: null,
    orgUnit: "/Employees",
    mainText: "<p>Erik was here</p>",
    gitHub: "https://github.com/Erma32",
    twitter: "https://x.com/erma32",
    stackOverflow: null,
    linkedIn: "/in/erik-gullberg-a57b70167/",
    imagePortraitUrl: null,
    imageWallOfLeetUrl: null,
    highlighted: false,
    published: false,
    primaryRole: "Fullstack Engineer",
    secondaryRole: null,
    area: "Engineering",
  };

  it("renders employee information correctly", () => {
    render(<EmployeeCard employee={testEmployee} displayMode={"grid"} />);

    expect(screen.getByText(testEmployee.name)).toBeInTheDocument();

    expect(
      screen.getByText(`Office: ${testEmployee.office}`),
    ).toBeInTheDocument();

    const linkedInLink = screen.getByRole("link", { name: "LinkedIn" });
    expect(linkedInLink).toBeInTheDocument();
    expect(linkedInLink).toHaveAttribute(
      "href",
      `https://linkedin.com/${testEmployee.linkedIn}`,
    );

    const githubLink = screen.getByRole("link", { name: "GitHub" });
    expect(githubLink).toBeInTheDocument();
    expect(githubLink).toHaveAttribute(
      "href",
      `https://github.com/${testEmployee.gitHub}`,
    );

    const twitterLink = screen.getByRole("link", { name: "X" });
    expect(twitterLink).toBeInTheDocument();
    expect(twitterLink).toHaveAttribute(
      "href",
      `https://x.com/${testEmployee.twitter}`,
    );
  });

  it("renders the fallback image if no portrait is available", () => {
    const employeeWithoutImage: Employee = {
      ...testEmployee,
      imagePortraitUrl: null,
    };
    render(
      <EmployeeCard employee={employeeWithoutImage} displayMode={"grid"} />,
    );
    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
