"use client";
import Employee from "@/types/employee";
import styles from "./page.module.css";
import EmployeeCard from "@/components/EmployeeCard/EmployeeCard";
import { ChangeEvent, useState } from "react";

export default function EmployeeList({ employees }: { employees: Employee[] }) {
  const [displayMode, setDisplayMode] = useState<"grid" | "spotlight">("grid");

  const handleDisplayModeChange = (event: ChangeEvent<HTMLInputElement>) => {
    setDisplayMode(event.target.value as "grid" | "spotlight");
  };

  return (
    <div className={styles.container}>
      <div className={styles.controls}>
        <label>
          <input
            type="radio"
            value="grid"
            checked={displayMode === "grid"}
            onChange={handleDisplayModeChange}
          />{" "}
          Grid
        </label>

        <label>
          <input
            type="radio"
            value="spotlight"
            checked={displayMode === "spotlight"}
            onChange={handleDisplayModeChange}
          />{" "}
          Spotlight
        </label>
      </div>
      <ul
        className={
          displayMode === "grid"
            ? styles.gridEmployeeContainer
            : styles.spotlightEmployeeContainer
        }
      >
        {employees.map((employee: Employee) => (
          <EmployeeCard
            employee={employee}
            displayMode={displayMode}
            key={employee.email}
          />
        ))}
      </ul>
    </div>
  );
}
