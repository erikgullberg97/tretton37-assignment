import Employee from "@/types/employee";
import Image from "next/image";
import omgCat from "../../../public/omgCat.jpg";
import styles from "./page.module.css";
import LinkedInIcon from "@/components/Icons/LinkedInIcon";
import GithubIcon from "@/components/Icons/GithubIcon";
import TwitterIcon from "@/components/Icons/TwitterIcon";

export default function EmployeeCard({
  employee,
  displayMode,
}: {
  employee: Employee;
  displayMode: string;
}) {
  const isGrid = displayMode === "grid";

  return (
    <div
      className={`${styles.container} ${isGrid ? styles.gridContainer : ""}`}
      data-testid="employee-card"
    >
      <Image
        className={styles.profileImage}
        height={isGrid ? 200 : 500}
        width={isGrid ? 150 : 375}
        src={employee.imagePortraitUrl ?? omgCat}
        alt={"Portrait picture of " + employee.name}
      ></Image>
      <div className={styles.employeeInfo}>
        <div>
          <p>{employee.name}</p>
          <p>Office: {employee.office ?? "-"}</p>
        </div>
        <div className={styles.links}>
          {employee.linkedIn ? (
            <a href={`https://linkedin.com/${employee.linkedIn}`}>
              <LinkedInIcon color="#65c27e" aria-label="LinkedIn" />
            </a>
          ) : (
            <LinkedInIcon color="gray" aria-label="LinkedIn" />
          )}
          {employee.gitHub ? (
            <a href={`https://github.com/${employee.gitHub}`}>
              <GithubIcon color="#65c27e" aria-label="GitHub" />
            </a>
          ) : (
            <GithubIcon color="gray" aria-label="GitHub" />
          )}
          {employee.twitter ? (
            <a href={`https://x.com/${employee.twitter}`}>
              <TwitterIcon color="#65c27e" aria-label="X" />
            </a>
          ) : (
            <TwitterIcon color="gray" aria-label="X" />
          )}
        </div>
      </div>
    </div>
  );
}
